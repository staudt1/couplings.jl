
module Couplings

using LinearAlgebra
using LoopVectorization


"""
Abstract projection target.
"""
abstract type ProjectionTarget end

"""
The full simplex as projection target.
"""
struct Simplex <: ProjectionTarget end

"""
A coupling with two possibly different marginals as projection target.
"""
struct Coupling <: ProjectionTarget end

"""
A coupling with identical marginals as projection target.
"""
struct SelfCoupling <: ProjectionTarget end

"""
Abstract projection metric.
"""
abstract type ProjectionMetric end

"""
L1 projection metric.
"""
struct L1 <: ProjectionMetric end

"""
L2 projection metric.
"""
struct L2 <: ProjectionMetric end

"""
Kullback leibler divergence as projection "metric".
"""
struct KL <: ProjectionMetric end

"""
Abstract algorithm that projects a matrix onto a projection target `T` along the
projection metric `M`.
"""
abstract type Algorithm{T <: ProjectionTarget, M <: ProjectionMetric} end

"""
    target(algo :: Algorithm)

Return the projection target of `algo`.
"""
target(algo::Algorithm{T}) where {T} = T()

"""
    metric(algo :: Algorithm)

Return the projection metric of `algo`.
"""
metric(algo::Algorithm{T, M}) where {T, M} = M()

"""
Result type when algorithms are applied via [`runalgorithm`](@ref).
"""
abstract type Projection end

"""
    matrix(r :: Projection)

Return the matrix corresponding to the projection result.
"""
matrix(r::Projection) = error("not implemented")

"""
    potentials(r :: Projection)

Return the dual potentials of the projection result.
"""
potentials(r::Projection) = error("not implemented")

"""
    marginals(r :: Projection)

Return the marginals of the projection result.
"""
marginals(r::Projection) = error("not implemented")

"""
    iterations(r :: Projection)

Return the number of iterations needed to calculate the projection result.
"""
iterations(r::Projection) = error("not implemented")

"""
    supportcount(r :: Projection)

Return the number of nonzero entries in the projection result.
"""
supportcount(r::Projection) = error("not implemented")

"""
    problemsize(r :: Projection)

Return the size of the full matrix.
"""
problemsize(r::Projection) = error("not implemented")

"""
    runtime(r :: Projection)

Runtime that was needed to create `r` in seconds.
"""
runtime(r::Projection) = error("not implemented")

"""
    err(r :: Projection)

Error estimate.
"""
err(r::Projection) = error("not implemented")

"""
    sparsity(r :: Projection)

Sparsity of 
"""
sparsity(r::Projection) = 1 - supportcount(r) / prod(problemsize(r))

"""
    runalgorithm(algo :: Algorithm{Simplex}, x)

Run a simplex projection algorithm `algo` on input `x`.
"""
runalgorithm(algo::Algorithm{Simplex}, x) = error("not implemented")

"""
    runalgorithm(algo :: Algorithm{Copuling}, x, ma, mb)

Run a coupling projection algorithm `algo` on input `x` with target marginals
`ma` and `mb`.
"""
runalgorithm(algo::Algorithm{Coupling}, x, ma, mb) = error("not implemented")
runalgorithm(algo::Algorithm{Coupling}, x, ma) = runalgorithm(algo, x, ma, ma)

"""
    runalgorithm(algo :: Algorithm{SelfCopuling}, x, m)

Run a self-coupling projection algorithm `algo` on input `x` with target
marginal `m`.
"""
runalgorithm(algo::Algorithm{SelfCoupling}, x, m) = error("not implemented")

include("sbfgs.jl")

include("precompile.jl")

end # module Coupling

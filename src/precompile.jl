
using PrecompileTools

@compile_workload begin

  for F in [Float32, Float64]
    x = rand(5, 5)
    r = SBFGS(x, precision = F)
  end

end

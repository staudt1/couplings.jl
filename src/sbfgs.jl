
"""
The SBFGS algorithm for L2 projections onto the space of self-couplings.

# Arguments
- `maxiter = 100`: Maximal number of iterations.
- `tolerance = 1e-5`: Iterations stop if the error drops below this value.
- `precision = Float64`: Floating point type.
"""
@kwdef struct SBFGS <: Algorithm{SelfCoupling, L2}
  maxiter::Int = 100
  tolerance::Float64 = 1e-5
  precision::Type{<:AbstractFloat} = Float64
end

function SBFGS(x, args...; kwargs...)
  algo = SBFGS(; kwargs...)
  return runalgorithm(algo, x, args...)
end

function (algo::SBFGS)(args...; kwargs...)
  return runalgorithm(algo, args...; kwargs...)
end

"""
Auxiliary structure that holds two vectors as well as their concatenation.
"""
struct StackedVar{T}
  a::T
  b::T
  ab::T
end

"""
    StackedVar(a, b)
    StackedVar(ab)

Create a `StackedVar` object, either based on two vectors `a` and `b` of equal
size or one stacked vector `ab` of even size.
"""
function StackedVar(a::AbstractVector, b::AbstractVector)
  return StackedVar(a, b, [a; b])
end

function StackedVar(ab::AbstractVector)
  @assert length(ab) % 2 == 0
  n = div(length(ab), 2)
  return StackedVar(ab[1:n], ab[(n + 1):(2n)], ab)
end

"""
Result object of the SBFGS algorithm.

Stores the input matrix, input marginal, the found potentials, as well as meta
information about the run.
"""
struct SBFGSProjection{K, T} <: Projection
  x::K              # input matrix
  m::StackedVar{T}  # marginals
  p::StackedVar{T}  # potentials

  error::Float64
  runtime::Float64
  iterations::Int
  nnz::Int
end

matrix(r::SBFGSProjection) = max.(0, r.x .- r.p.a .- r.p.b')
potentials(r::SBFGSProjection) = (r.p.a, r.p.b)
marginals(r::SBFGSProjection) = r.m.a
problemsize(r::SBFGSProjection) = size(r.x)
supportcount(r::SBFGSProjection) = r.nnz

function runalgorithm(
  algo::SBFGS,
  x::AbstractArray,
  ma = ones(size(x, 1)),
  pa = nothing,
  pb = nothing,
)
  @assert size(x, 1) == size(x, 2)
  starttime = time_ns()

  # Potentially adapt the input argument
  x = initmatrix(algo, x)

  # Parameter initialization
  m = initmarginals(algo, x, ma)
  p = initpotentials(algo, x, pa, pb)
  iterations = algo.maxiter

  # Prepare first iteration
  g = gradientinfo(x, m, p)
  if g.nnz < max(1, 0.001 * length(x))
    @warn """
    Initial gradients are extremely sparse. Use smaller initial potentials \
    or shift the target matrix towards larger values.
    """
  end
  u = stepdirection(g)

  # Structured-BFGS steps
  for iter in 1:iterations
    p0, g0 = p, g

    tau = stepsize(x, p, g, u)
    p = updatepotentials(p, u, tau)
    g = gradientinfo(x, m, p)

    if isconverged(g, algo.tolerance)
      iterations = iter
      break
    end

    u = stepdirection(g, g0, p, p0)
  end

  runtime = Float64((time_ns() - starttime) / 1e9)
  error = Float64(sqrt(sum(abs2, g.grad.ab)))

  return SBFGSProjection(x, m, p, error, runtime, iterations, g.nnz)
end

function initmatrix(algo, x :: Matrix)
  @assert size(x, 1) == size(x, 2) """
  Expected a quadratic input matrix.
  """
  F = algo.precision
  return convert(Matrix{F}, x)
end

function initmarginals(algo, x::Matrix, ma)
  @assert length(ma) == size(x, 1) == size(x, 2)
  F = algo.precision
  ma = convert(Vector{F}, ma)
  return StackedVar(ma, ma)
end

function initpotentials(algo, x::Matrix, pa, pb)
  F = algo.precision
  n = size(x, 1)
  pa = isnothing(pa) ? zeros(F, n) : convert(Array{F}, pa)
  pb = isnothing(pb) ? zeros(F, n) : convert(Array{F}, pb)
  return StackedVar(pa, pb)
end

function gradientinfo(x::Matrix, m, p)
  grada = zeros(eltype(p.a), length(p.a))
  gradb = zeros(eltype(p.b), length(p.b))
  diaga = zeros(eltype(p.a), length(p.a))
  diagb = zeros(eltype(p.b), length(p.b))

  @tturbo for i in 1:length(p.a)
    for j in 1:length(p.b)
      val = max(0, x[i, j] - p.a[i] - p.b[j])
      grada[i] -= val
      diaga[i] += val > 0
    end
  end

  @tturbo for j in 1:length(p.b)
    for i in 1:length(p.a)
      val = max(0, x[i, j] - p.a[i] - p.b[j])
      gradb[j] -= val
      diagb[j] += val > 0
    end
  end

  nnz = round(Int, sum(diaga))
  diaga = max.(1, diaga)
  diagb = max.(1, diagb)

  grad = StackedVar(grada .+ m.a, gradb .+ m.b)
  lambda = StackedVar(1 ./ diaga, 1 ./ diagb)

  return (; grad, lambda, nnz)
end

function isconverged(g, tol)
  return sqrt(sum(abs2, g.grad.ab)) < tol
end

function stepdirection(g)
  u = .-g.lambda.ab .* g.grad.ab
  return calibrate(g, u)
end

function stepdirection(g, g0, p, p0)
  s = p.ab .- p0.ab
  y = g.grad.ab .- g0.grad.ab

  irho = dot(s, y)
  if irho < 0
    @warn "Curvature condition is violated"
  end
  rho = 1 / irho

  rsg = rho * dot(s, g.grad.ab)
  x = g.grad.ab .- rsg .* y
  x .= g.lambda.ab .* x
  ryx = rho * dot(y, x)
  x .= x .- ryx * s
  u = .-x .- rsg .* s

  return calibrate(g, u)
end

function calibrate(g, u)
  unorm = sqrt(sum(abs2, u))
  gnorm = sqrt(sum(abs2, g.grad.ab))

  cosine = -dot(u, g.grad.ab) / gnorm / unorm
  if cosine < 1 / length(u)
    @warn "Cosine condition is violated"
    u = -g.lambda.ab .* g.grad.ab
  end

  return StackedVar(u)
end

function stepsize(x::Matrix{F}, p, g, u) where {F}
  d1 = dot(g.grad.ab, u.ab)
  d2 = zero(F)
  @tturbo for i in 1:length(p.a)
    for j in 1:length(p.b)
      val = max(0, x[i, j] - p.a[i] - p.b[j])
      d2 += (val > 0) * (u.a[i] + u.b[j])^2
    end
  end
  return -d1 / d2
end

function updatepotentials(d, u, tau)
  da = d.a .+ tau .* u.a
  db = d.b .+ tau .* u.b
  return StackedVar(da, db)
end

##
## Support for matrices declared as symmetric
##

function gradientinfo(x::Symmetric{F, Matrix{F}}, m, p) where {F}
  x = x.data
  grada = zeros(eltype(p.a), length(p.a))
  diaga = zeros(eltype(p.a), length(p.a))

  @tturbo for i in 1:length(p.a)
    for j in 1:length(p.a)
      val = max(0, x[i, j] - p.a[i] - p.a[j])
      grada[i] -= val
      diaga[i] += val > 0
    end
  end

  nnz = round(Int, sum(diaga))
  diaga = max.(1, diaga)

  grad = StackedVar(grada .+ m.a, grada .+ m.a)
  lambda = StackedVar(one(F) ./ diaga, one(F) ./ diaga)

  return (; grad, lambda, nnz)
end

function stepsize(x::Symmetric{F, Matrix{F}}, p, g, u) where {F}
  return stepsize(x.data, p, g, u)
end

function initmatrix(algo, x :: Symmetric{F, <: AbstractMatrix{F}}) where {F}
  @assert F == algo.precision """
  Desired precision $(algo.precision) not compatible with input precision $F.
  """
  return x
end

function initmarginals(algo, x::Symmetric{F, <:AbstractMatrix{F}}, ma) where {F}
  return initmarginals(algo, x.data, ma)
end

function initpotentials(
  algo,
  x::Symmetric{F, <:AbstractMatrix{F}},
  pa,
  pb,
) where {F}
  return initpotentials(algo, x.data, pa, pb)
end

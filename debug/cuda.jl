
using Revise
using LinearAlgebra
using CUDA
using Couplings

import Couplings: StackedVar, SBFGS

function Couplings.initmarginals(algo :: SBFGS, x :: CuMatrix, ma)
  F = algo.precision
  ma = convert(CuVector{F}, ma)
  StackedVar(ma, ma)
end

function Couplings.initpotentials(algo, x :: CuMatrix, pa, pb)
  F = algo.precision
  n = size(x, 1)
  pa = isnothing(pa) ? zeros(F, n) : pa
  pb = isnothing(pb) ? zeros(F, n) : pb

  pa = convert(CuArray{F}, pa)
  pb = convert(CuArray{F}, pb)
  StackedVar(pa, pb)
end

function gradientinfo_kernel_a!(grada, diaga, x, pa, pb)
  start = (blockIdx().x - 1) * blockDim().x + threadIdx().x
  stride = gridDim().x * blockDim().x
  for i in start:stride:length(pa)
    for j in 1:length(pb)
      val = max(0, x[i,j] - pa[i] - pb[j])
      grada[i] -= val
      diaga[i] += val > 0
    end
  end
  nothing
end

# TODO: this kernel is badly optimized
function gradientinfo_kernel_b!(gradb, diagb, x, pa, pb)
  start = (blockIdx().x - 1) * blockDim().x + threadIdx().x
  stride = gridDim().x * blockDim().x
  for j in start:stride:length(pb)
    for i in 1:length(pa)
      val = max(0, x[i,j] - pa[i] - pb[j])
      gradb[j] -= val
      diagb[j] += val > 0
    end
  end
  nothing
end

function Couplings.gradientinfo(x :: CuMatrix, m, p)
  grada = convert(CuVector, zeros(eltype(p.a), length(p.a)))
  gradb = convert(CuVector, zeros(eltype(p.b), length(p.b)))
  diaga = convert(CuVector, zeros(eltype(p.a), length(p.a)))
  diagb = convert(CuVector, zeros(eltype(p.b), length(p.b)))

  cua = @cuda launch = false gradientinfo_kernel_a!(grada, diaga, x, p.a, p.b)
  cub = @cuda launch = false gradientinfo_kernel_b!(gradb, diagb, x, p.a, p.b)

  threads = 512
  blocks = cld(length(p.a), threads)
  cua(grada, diaga, x, p.a, p.b; threads, blocks)
  cub(gradb, diagb, x, p.a, p.b; threads, blocks)

  nnz = round(Int, sum(diaga))
  diaga = max.(1, diaga)
  diagb = max.(1, diagb)

  grad = StackedVar(grada .+ m.a, gradb .+ m.b)
  lambda = StackedVar(1 ./ diaga, 1 ./ diagb)

  (; grad, lambda, nnz)
end

function Couplings.gradientinfo(x :: Symmetric{F, <: CuMatrix{F}}, m, p) where {F}
  x = x.data
  grada = convert(CuVector, zeros(eltype(p.a), length(p.a)))
  diaga = convert(CuVector, zeros(eltype(p.a), length(p.a)))

  cua = @cuda launch = false gradientinfo_kernel_a!(grada, diaga, x, p.a, p.a)

  threads = 512
  blocks = cld(length(p.a), threads)
  cua(grada, diaga, x, p.a, p.a; threads, blocks)

  nnz = round(Int, sum(diaga))
  diaga = max.(1, diaga)

  grad = StackedVar(grada .+ m.a, grada .+ m.a)
  lambda = StackedVar(1 ./ diaga, 1 ./ diaga)

  (; grad, lambda, nnz)
end

function stepsize_kernel!(buf, x, pa, pb, ua, ub)
  start = (blockIdx().x - 1) * blockDim().x + threadIdx().x
  stride = gridDim().x * blockDim().x
  for i in start:stride:length(pa)
    for j in 1:length(pb)
      val = max(0, x[i,j] - pa[i] - pb[j])
      buf[i] += (val > 0) * (ua[i] + ub[j])^2
    end
  end
  nothing
end

function Couplings.stepsize(x :: CuMatrix, p, g, u)
  d1 = dot(g.grad.ab, u.ab)
  d2vals = convert(CuVector, zeros(eltype(p.a), length(p.a)))

  cu = @cuda launch = false stepsize_kernel!(d2vals, x, p.a, p.b, u.a, u.b)

  threads = 512
  blocks = cld(length(p.a), threads)
  cu(d2vals, x, p.a, p.b, u.a, u.b; threads, blocks)

  - d1 / sum(d2vals)
end

function Couplings.stepsize(x :: Symmetric{F, <: CuMatrix{F}}, p, g, u) where {F}
  Couplings.stepsize(x.data, p, g, u)
end


n = 5000
K = (0.05 .- rand(Float32, n, n)) * 1000
# m = StackedVar(rand(n))
# p = StackedVar(rand(n))

# cK = convert(CuArray, K)
# cm = StackedVar(convert(CuArray, m.ab))
# cp = StackedVar(convert(CuArray, p.ab))


using BenchmarkTools

#@time res1 = Couplings.runalgorithm(Couplings.SBFGS(tolerance = 1e-4), K)
@time res = Couplings.runalgorithm(Couplings.SBFGS(maxiter = 110, tolerance = 1e-4), cK)
println()
#@time res1 = Couplings.runalgorithm(Couplings.SBFGS(tolerance = 1e-4), K)
@time res2 = Couplings.runalgorithm(Couplings.SBFGS(tolerance = 1e-4), cK)

# CUDA.@sync Couplings.gradientinfo(cK, cm, cp)

# @btime CUDA.@sync Couplings.gradientinfo($cK, $cm, $cp)
# @btime CUDA.@sync Couplings.gradientinfo($(Symmetric(cK)), $cm, $cp)
# @btime Couplings.gradientinfo($K, $m, $p)
# @btime Couplings.gradientinfo($(Symmetric(K)), $m, $p)

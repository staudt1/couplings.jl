# Couplings.jl

Efficient implementations of fast algorithms to project matrices onto coupling
spaces.

This package is currently under development. In the future, projection onto the
simplex, onto general couplings, and onto self-couplings will be supported.

## Installation

This package is currently not officially registered. Run
```julia
using Pkg; Pkg.add(url = "https://gitlab.gwdg.de/staudt1/couplings.jl.git")
```
in a julia shell to install it.

## Usage

Currently, only the SBFGS algorithm described by [Chu et. al. 2023](https://ojs.aaai.org/index.php/AAAI/article/view/25877) is included. It can be used for projections of matrices onto the space of self-couplings with a given marginal. It works best if the marginal is (close to) uniform.

```julia
using Couplings

# Matrix to be projected onto the doubly stochastic matrices
x = rand(5, 5)

# Projection result object
r = Couplings.SBFGS(x, maxiter = 100, threshold = 1e-5)

@show Couplings.matrix(r) # Actual projected matrix
@show Couplings.iterations(r)
@show Couplings.sparsity(r)
```

**Note:** Due to the zero default initialization of the potentials, the matrix `x` in the code above should not only have negative entries. If in doubt, add a constant to create sufficiently many non-negatives.

